
Blockly.Blocks['gain_set'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Gain");
        this.appendDummyInput()
            .appendField(new Blockly.FieldDropdown([
                ['left', 'LEFT'],
                ['right', 'RIGHT']
            ]), 'CHANNEL');
        this.appendValueInput('VALUE')
            .setCheck('Number');
        //this.setOutput(true, 'Number');
        this.setTooltip('Sets volume.');
        this.setNextStatement(true);
        this.setPreviousStatement(true);
        this.inputsInline = true;
    }
};

Blockly.Blocks['freq_set'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Frequency");
        this.appendDummyInput()
            .appendField(new Blockly.FieldDropdown([
                ['left', 'LEFT'],
                ['right', 'RIGHT']
            ]), 'CHANNEL');
        this.appendValueInput('VALUE')
            .setCheck('Number');
        this.inputsInline = true;
        this.setTooltip('Sets frequency.');
        this.setNextStatement(true);
        this.setPreviousStatement(true);
    }
};

Blockly.Blocks['lfo_set'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("LFO");
        this.appendDummyInput()
            .appendField(new Blockly.FieldDropdown([
                ['left', 'LEFT'],
                ['right', 'RIGHT']
            ]), 'CHANNEL');
        this.appendValueInput('VALUE')
            .setCheck('Number');
        this.inputsInline = true;
        this.setTooltip('Sets LFO.');
        this.setNextStatement(true);
        this.setPreviousStatement(true);
    }
};

Blockly.Blocks['preset_load'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Load preset:");
        this.appendValueInput('NAME')
            .setCheck('String');
        this.inputsInline = true;
        this.setTooltip('Load a preset');
        this.setNextStatement(true);
        this.setPreviousStatement(true);
    }
};

Blockly.defineBlocksWithJsonArray([{
    "type": "timer_delay",
    "message0": " wait %1 milliseconds",
    "args0": [{
        "type": "field_number",
        "name": "DELAYMS",
        "min": 0,
        "max": 60000,
        "value": 1000
    }],
    "previousStatement": null,
    "nextStatement": null
}]);

Blockly.JavaScript['timer_delay'] = function (block) {
    var ms = Number(block.getFieldValue('DELAYMS'));
    var code = 'waitForMilliseconds(' + ms + ');\n';
    return code;
};

Blockly.JavaScript['gain_set'] = function (block) {
    var channel = block.getFieldValue("CHANNEL");
    if (channel == "LEFT") { channel = "left"; } else { channel = "right"; }
    var x = Blockly.JavaScript.valueToCode(block, 'VALUE', Blockly.JavaScript.ORDER_ATOMIC);
    var code = 'control("' + channel + 'Gain", ' + x + ');\n';
    return code;
};

Blockly.JavaScript['freq_set'] = function (block) {
    var channel = block.getFieldValue("CHANNEL");
    if (channel == "LEFT") { channel = "left"; } else { channel = "right"; }
    var x = Blockly.JavaScript.valueToCode(block, 'VALUE', Blockly.JavaScript.ORDER_ATOMIC);
    var code = 'control("' + channel + 'Frequency", ' + x + ');\n';
    return code;
};

Blockly.JavaScript['lfo_set'] = function (block) {
    var channel = block.getFieldValue("CHANNEL");
    if (channel == "LEFT") { channel = "left"; } else { channel = "right"; }
    var x = Blockly.JavaScript.valueToCode(block, 'VALUE', Blockly.JavaScript.ORDER_ATOMIC);
    var code = 'control("' + channel + 'LFO", ' + x + ');\n';
    return code;
};

Blockly.JavaScript['preset_load'] = function (block) {
    var name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
    var code = 'control("loadPreset", ' + name + ');\n';
    return code;
};

Blockly.JavaScript.STATEMENT_PREFIX = 'highlightBlock(%1);\n';
Blockly.JavaScript.addReservedWords('highlightBlock');

var workspace = Blockly.inject('blocklyDiv',
    { toolbox: document.getElementById('toolbox') });

function initApi(interpreter, globalObject) {
    var wrapper = function (varname, value) {
        return x.control(varname, value);
    };
    interpreter.setProperty(globalObject, 'control',
        interpreter.createNativeFunction(wrapper));

    initInterpreterWaitForMilliseconds(interpreter, globalObject);// Add an API function for highlighting blocks.

    interpreter.setProperty(globalObject, 'highlightBlock',
        interpreter.createNativeFunction(function (id) {
            return workspace.highlightBlock(id);
        }));
}

function initInterpreterWaitForMilliseconds(interpreter, globalObject) {
    // Ensure function name does not conflict with variable names.
    Blockly.JavaScript.addReservedWords('waitForMilliseconds');

    var wrapper = interpreter.createAsyncFunction(
        function (timeInMilliseconds, callback) {
            // Delay the call to the callback.
            setTimeout(callback, timeInMilliseconds);
        });
    interpreter.setProperty(globalObject, 'waitForMilliseconds', wrapper);
}
function run() {
    var latestCode = Blockly.JavaScript.workspaceToCode(workspace);
    console.log(latestCode);
    var z = new Interpreter(latestCode, initApi);

    runner = function () {
        if (z) {
            var hasMore = z.run();
            if (hasMore) {
                // Execution is currently blocked by some async call.
                // Try again later.
                setTimeout(runner, 10);
            } else {
                // Program is complete.
                console.log("execution complete");
                z = null;
            }
        }
    };
    runner();


}