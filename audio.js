
function ToneGenerator() {
    var self = this;
    self.dataChannel = new DataChannel();
    self.ac = new (window.AudioContext || window.webkitAudioContext)();

    self.initialSettings = {
        'leftGain': 0,
        'rightGain': 0,
        'leftFrequency': 300,
        'rightFrequency': 600,
        'leftLFO': 0,
        'rightLFO': 0
    };

    self.init = function () {

        self.dataChannel.dataReceived = function (data) {
            switch (data.command) {
                case 'control':
                    self.control(data.varname, data.value);
                    break;
                case 'preset':
                    self.loadPreset(data.name, data.params);
                    break;
            }  
        };

        self.presets = JSON.parse(localStorage.getItem("presets")) || {};
        self.createPresetButtons();
        // create base oscillators and gain nodes etc
        self.oscillatorLeft = self.ac.createOscillator();
        self.oscillatorLeft.type = 'square';
        self.oscillatorLeft.frequency.value = self.initialSettings['leftFrequency']; // value in hertz

        self.oscillatorRight = self.ac.createOscillator();
        self.oscillatorRight.type = 'square';
        self.oscillatorRight.frequency.value = self.initialSettings['rightFrequency']; // value in hertz


        self.gainLeft = self.ac.createGain();
        self.gainLeft.gain.value = self.initialSettings['leftGain'];
        self.gainLeft2 = self.ac.createGain();
        self.gainLeft2.gain.value = 1;

        self.gainRight = self.ac.createGain();
        self.gainRight.gain.value = self.initialSettings['rightGain'];
        self.gainRight2 = self.ac.createGain();
        self.gainRight2.gain.value = 1;

        self.panLeft = self.ac.createStereoPanner();
        self.panLeft.pan.setValueAtTime(-1, self.ac.currentTime);
        self.panRight = self.ac.createStereoPanner();
        self.panRight.pan.setValueAtTime(1, self.ac.currentTime);

        self.lfoLeft = self.ac.createOscillator();
        self.lfoLeft.frequency.value = self.initialSettings['leftLFO'];
        self.lfoRight = self.ac.createOscillator();
        self.lfoRight.frequency.value = self.initialSettings['rightLFO'];

        self.lfoLeftGain = self.ac.createGain();
        self.lfoLeftGain.gain.value = 1;
        self.lfoLeft.connect(self.lfoLeftGain);

        self.lfoRightGain = self.ac.createGain();
        self.lfoRightGain.gain.value = 1;
        self.lfoRight.connect(self.lfoRightGain);

        // wire it all up
        self.oscillatorLeft
            .connect(self.panLeft)
            .connect(self.gainLeft)
            .connect(self.gainLeft2)
            .connect(self.ac.destination);
        self.oscillatorRight
            .connect(self.panRight)
            .connect(self.gainRight)
            .connect(self.gainRight2)
            .connect(self.ac.destination);

    }

    self.playing = false;

    self.play = function () {
        if (!self.playing) {
            self.oscillatorLeft.start();
            self.oscillatorRight.start();

            self.lfoLeft.start();
            self.lfoRight.start();
        } else {
            self.initialSettings = self.getSettings();
            self.oscillatorLeft.stop();
            self.oscillatorRight.stop();
            self.lfoLeft.stop();
            self.lfoRight.stop();
            self.init();
        }
        self.playing = !self.playing;
    }

    self.setFromSlider = function (param, value) {
        if (!self.dataChannel.isController) {
            param.setValueAtTime(value, self.ac.currentTime);
        }
    }

    self.control = function (varname, value, send = true) {
        var slider = document.getElementById(varname);
        if (slider != null && slider.value != value) {
            slider.value = value;
        }

        if (!self.dataChannel.isController) {
            switch (varname) {
                case "leftGain":
                    self.setFromSlider(self.gainLeft.gain, slider.value);
                    break;
                case "rightGain":
                    self.setFromSlider(self.gainRight.gain, slider.value);
                    break;
                case "leftFrequency":
                    self.setFromSlider(self.oscillatorLeft.frequency, slider.value);
                    break;
                case "rightFrequency":
                    self.setFromSlider(self.oscillatorRight.frequency, slider.value);
                    break;
                case "leftLFO":
                    self.setLFOLeftFrequency(slider.value);
                    break;
                case "rightLFO":
                    self.setLFORightFrequency(slider.value);
                    break;
                case "loadPreset":
                    self.loadPreset(value, self.presets[value]);
                    break;
            }
        }
        else
        {
            if (send) {
                self.dataChannel.sendData({ command: 'control', varname: varname, value: value });
            }
        }

    }

    self.setLFOLeftFrequency = function (f) {
        if (f == 0) {
            self.lfoLeftGain.disconnect();
            self.lfoLeftConnected = false;
            self.setFromSlider(self.gainLeft2.gain, 1);
        } else {
            if (!self.lfoLeftConnected) {
                self.lfoLeftGain.connect(self.gainLeft2.gain);
                self.lfoLeftConnected = true;
            }
            self.setFromSlider(self.lfoLeft.frequency, f);
        }
    }

    self.setLFORightFrequency = function(f) {
        if (f == 0) {
            self.lfoRightGain.disconnect();
            self.lfoRightConnected = false;
            self.setFromSlider(self.gainRight2.gain, 1);
        } else {
            if (!self.lfoRightConnected) {
                self.lfoRightGain.connect(self.gainRight2.gain);
                self.lfoRightConnected = true;
            }
            self.setFromSlider(self.lfoRight.frequency, f);
        }
    }

    self.getSettings = function() {
        return {
            'leftGain': document.getElementById('leftGain').value,
            'rightGain': document.getElementById('rightGain').value,
            'leftFrequency': document.getElementById('leftFrequency').value,
            'rightFrequency': document.getElementById('rightFrequency').value,
            'leftLFO': document.getElementById('leftLFO').value,
            'rightLFO': document.getElementById('rightLFO').value
        };
    }

    self.savePreset = function() {
        var name = document.getElementById("presetName").value;
        self.presets[name] = getSettings();
        localStorage.setItem("presets", JSON.stringify(self.presets));
        createPresetButtons();
    }

    self.loadPreset = function (name, settings) {
        var t = document.getElementById("presetName");
        t.value = name;
        if (!self.dataChannel.isController) {
            for (let key in settings) {
                self.control(key, settings[key]);
            }
        }
        else {
            self.dataChannel.sendData({ command: 'preset', name: name, params: settings });

            for (let key in settings) {
                self.control(key, settings[key], false);
            }
        }
    }

    self.createPresetButtons = function () {
        var d = document.getElementById("presets");
        d.innerHTML = "";
        for (let key in self.presets) {
            var b = document.createElement("button");
            b.innerHTML = key;
            b.onclick = function () { self.loadPreset(key, self.presets[key]); }
            d.appendChild(b);
        }
    }
}
